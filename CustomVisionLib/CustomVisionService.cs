﻿namespace CustomVisionLib
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Linq;
	using System.Net.Http;
	using System.Threading.Tasks;
	using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training;
	using Microsoft.Azure.CognitiveServices.Vision.CustomVision.Training.Models;

	public class CustomVisionService
	{
		public event Action<string>? OnProgress;
		private readonly string endpoint;
		private readonly Guid projectId;
		private readonly string trainingKey;
		private readonly ICustomVisionTrainingClient client;

		public CustomVisionService(string endpoint, Guid projectId, string trainingKey)
		{
			this.client = CreateClient(trainingKey, endpoint);
			this.endpoint = endpoint;
			this.projectId = projectId;
			this.trainingKey = trainingKey;
		}

		private static CustomVisionTrainingClient CreateClient(string trainingKey, string endpoint)
		{
			var cvClient =
				new CustomVisionTrainingClient(
					new ApiKeyServiceClientCredentials(trainingKey), new HttpClient(), true) {
					Endpoint = new Uri(endpoint).ToString()
				};
			return cvClient;
		}
		public async Task<int> CountImages()
		{
			var cvResult = (await client.GetImagesWithHttpMessagesAsync(projectId)).Body;
			return cvResult.Count;
		}

		public async Task GetStats()
		{
			var cvResult = (await client.GetImagesWithHttpMessagesAsync(projectId)).Body;
			OnProgress?.Invoke($"Images' metadata received, analyzing...");
			var http = new HttpClient();
			var totalSize = (await cvResult
				.AsParallel()
				.Select(async img =>
				{
					await using var netStream = await http.GetStreamAsync(img.OriginalImageUri);
					using var ms = new MemoryStream();
					await netStream.CopyToAsync(ms);
					return ms.Length;
				})
				.WhenAll())
				.Aggregate((curr, next) => curr + next);
			OnProgress?.Invoke($"Got {cvResult.Count} images for a total of {totalSize}B or {totalSize / (1024 * 1024)}MiB");
		}

		public async Task AnalyseImage(Guid imageId)
		{
			var image = (await client.GetImagesByIdsAsync(projectId, new List<Guid> { imageId })).First();
			OnProgress?.Invoke($"Id: {image.Id}");
			OnProgress?.Invoke($"Regions found: {image.Regions.Count}");
			foreach (var region in image.Regions.OrderBy(r => r.Left))
			{
				OnProgress?.Invoke($"{region.Left:F4}: {region.TagName}");
			}
		}

		public async Task DownloadImages(string downloadFolder)
		{
			OnProgress?.Invoke($"Downloading to {downloadFolder}");
			Directory.CreateDirectory(downloadFolder);
			var http = new HttpClient();
			var count = 0;
			await (await client.GetImagesAsync(projectId)) // Download Metadata about all images
				.AsParallel()
				.Select(image => DownloadImageAsync(image)).WhenAll(); // Download each image
			OnProgress?.Invoke($"Downloaded {count} images to {downloadFolder}");

			async Task DownloadImageAsync(Image img)
			{
				var fullPath = Path.Combine(downloadFolder, img.Id.ToString()) + ".jpg";

				await using var netStream = await http.GetStreamAsync(img.OriginalImageUri);
				await using var fileStream = File.OpenWrite(fullPath);
				await netStream.CopyToAsync(fileStream);
				count++;
			}
		}
	}
}
